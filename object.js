//create object 
var obj = {str: 'text', num: 1, bool: true, arr: [1,2,3]}

// turn object into string
var strobj = JSON.stringify(obj)
console.log(strobj)

//parse JSON string into object
var parsedstrobj = JSON.parse(strobj)
console.log(parsedstrobj)